
from rest_framework import serializers
from author.models import Author


class MySerializer(serializers.ModelSerializer):
	class Meta:
		model = Author
		fields = ('first_name','last_name', 'email', 'avatar')

