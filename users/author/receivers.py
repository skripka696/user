from django.dispatch import receiver
from django.db.models.signals import post_save
import time


@receiver
def example_signal_receivers(sender, **kwargs):
    #print kwargs['arg1']
    #print kwargs['boo']
    #print "Hello"
    #print kwargs['a']
    pass

@receiver(post_save)
def some_data_saved(sender, **kwargs):
    print('Database changed')    


@receiver(post_save)
def add_time(sender, **kwargs):
    ticks = time.time()
    print ticks

