from django.apps import AppConfig


class AuthorConfig(AppConfig):
    name = 'author'
    verbose_name = "Author"


    def ready(self):
        #import signals handlers
        #from critic import receiver
        #import reader.signals
        from author import receivers 
        #import critic.receivers
