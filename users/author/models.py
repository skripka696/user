from django.db import models
#from django.contrib.auth.models import User

class Author(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField()

 	avatar = models.ImageField(upload_to = 'avatars/', default="index.jpg")

	def __unicode__(self):
		return '{0} {1} {2} {3}'.format(self.first_name, self.last_name, self.email, self.avatar)

class Book(models.Model):
	author = models.ForeignKey(Author)
	name = models.CharField(max_length=100)
	
	def __unicode__(self):
		return '{0} - {1}'.format(self.author, self.name)

class Reader(models.Model):
	name = models.CharField(max_length = 100)
	book = models.ManyToManyField(Book)

	def __unicode__(self):
		return '{0} {1}'.format(self.name, self.book)
