from django.contrib import admin


from author.models import  Author, Book, Reader


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('first_name','email', 'avatar')
    search_fields = ['first_name']
    list_filter = ['first_name']

class ReaderAdmin(admin.ModelAdmin):
    #filter_horizontal = ['book'] 
    filter_vertical = ['book'] 

admin.site.register(Author, AuthorAdmin)
admin.site.register(Book)
admin.site.register(Reader, ReaderAdmin)
