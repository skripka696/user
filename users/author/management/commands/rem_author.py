from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from author.models import Author

class Command(BaseCommand):
	help = "new Help"
	#option_list = BaseCommand.option_list+make_option('--delete',
	#			 action='store_true', dest='delete',
	#			 default=False,help="hElp")
	
	def add_arguments(self, parser):
		parser.add_argument('poll_id', nargs='+', type=int)
		parser.add_argument('--delete',
				 action='store_true', dest='delete',
				 default=False,help="hElp")

	def handle(self, *args, **options):
		#print kwargs
		if options['delete']:
			author = Author.objects.get(id =int(options['poll_id'][0]))
			print 'deleted'
			author.delete()
		else:
			print error