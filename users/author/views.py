# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, DetailView, ListView
from django.views.generic.edit import FormView, View, UpdateView
from author.models import  Author

from forms import AuthorForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

from author.forms import AuthorForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.sessions.models import Session
import json
import pytz
import datetime
from django.core import serializers
from django.views.decorators.cache import cache_page

class HomeView(TemplateView):
    template_name ='author/home.html'
    #template_name ='testapp/home2.html'
    #template_name ='testapp/home3.html'
    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        context['author_name'] = get_author()
            

        context['books'] = get_books()
        context['list'] = get_list()

        return context

def get_author_filter(request):
    #Author.filter_author.get_author()
    #if request.POST:
    form = AuthorForm()
    return HttpResponse(form)

class AuthorView(FormView):
    form_class = AuthorForm
    template_name = 'author/author.html'
    success_url = '/critic/thanks'
    #def post(self, request,*args,**kwargs):
 
    def form_valid(self,form):
        self.object = form.save()
        print self.object
        return super(AuthorView, self).form_valid(form)

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(AuthorView, self).dispatch(*args, **kwargs)


'''
class GetAuthorView(DetailView):
    model = Author
    template_name = 'author/author_view.html'

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(GetAuthorView, self).dispatch(*args, **kwargs)
'''

class GetAuthorView(UpdateView):
    model = Author
    template_name = 'author/author_view.html'
    fields = '__all__'
    success_url = '/get_list/'

    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        print self.object
        form_class = AuthorForm(instance=self.object)
        print form_class.fields
        author = Author.objects.get(first_name = 'Bob')
        print 1
        request.session['user_author'] = author.first_name
        kwargs.update({'user_author': author.first_name})
        print kwargs
        print 2
        # user_author = request.session['user_author']
        # print request.session['user_author']
        # return user_author
        return super(GetAuthorView, self).get(request, *args, **kwargs)

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(GetAuthorView, self).dispatch(*args, **kwargs)

    # def get(self, request, *args, **kwargs):
        # print 1


class GetAuthorList(ListView):
    model = Author
    template_name = 'author/author_view_list.html'

    def get_context_data(self, **kwargs):
        context = super(GetAuthorList, self).get_context_data(**kwargs)
        context.update({'user_author': self.request.session['user_author']})
        return context
    
    def post(self,request,*args,**kwargs):
        q = request.POST.get('search','Bill')
        u = request.POST.get('up','')
              
        search = Author.objects.filter(first_name__icontains=q)
        
        if u =='up':
            search = search.order_by('id')
        else:
            search = search.order_by('-id')
        # up = Author.objects.filter(first_name__icontains=q).order_by('first_name')
        # down = Author.objects.filter(first_name__icontains=q).order_by('-first_name')
        #search = Author.objects.filter(first_name__istartswith=q)
        return render(request,self.template_name,
            {'author_list': search})

   
    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(GetAuthorList, self).dispatch(*args, **kwargs)

 
    
'''

class GetAuthorSave(UpdateView):
    model = Author
    template_name = 'author/author_view.html'
    success_url = '/get_save/'
    
    


    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(GetAuthorSave, self).dispatch(*args, **kwargs)
 '''


class Regist(FormView):
    form_class = UserCreationForm
    success_url = '/get_save/'
    template_name = 'author/regist.html'

    def form_valid(self,form):
        self.object = form.save()
        return super(Regist, self).form_valid(form)


class Login(FormView):
    form_class = AuthenticationForm
    success_url = '/get_list/'
    template_name = 'author/login.html'

    def form_valid(self,form):
        user = form.get_user()
        login(self.request, user)
        return super(Login, self).form_valid(form)


class LogOut(View):
    success_url = '/regist/'
    template_name = 'author/base.html'

    def get(self,request):
        logout(request)
        return HttpResponseRedirect('/login')

class Avatar(FormView):
    template_name = 'author/avatar.html'
    form_class = AuthorForm
    success_url = '/get_save/'

    def form_valid(self, form):
        form.save()
        return super(Avatar, self).form_valid(form)

    def ses(request):
        form_list = []
        #request.session['avatar'] = avatar
        form_list = request.session['form_list']
        form_list.append(avatar)
        request.session['form_list'] = form_list

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(Avatar, self).dispatch(*args, **kwargs)


    

class ThanksView(TemplateView):
    template_name ='thanks/'

class IndexView(TemplateView):
    model = Author
    template_name = 'author/index.html'

class SaveView(TemplateView):
    model = Author
    template_name = 'author/save.html'


def set_timezone(request):
    template_name = 'author/set_timezone.html'
    if request.method == 'POST':
        request.session['django_timezone'] = request.POST['timezone']
        return redirect('/')
    else:
        return render(request,template_name,
                        {'timezones': pytz.common_timezones,
                        'TIME_ZONE':'Europe/Kiev', 'time': datetime.datetime.now()})
